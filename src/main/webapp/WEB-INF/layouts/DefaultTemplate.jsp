<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%--
  @author Vladimir Martynyuk
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<html>
<head>

    <title><tiles:getAsString name="title" /></title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round|Montserrat:400,700" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/libs/jstree/themes/default/style.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/glyphicons.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/default.css" />

    <link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono|Montserrat|Roboto|Open+Sans' rel='stylesheet' type='text/css'>

    <script src="${pageContext.request.contextPath}/resources/libs/jquery-1.10.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/libs/jstree/jstree.min.js"></script>

    <script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
</head>
<body>
<div id="header">
    <tiles:insertAttribute name="header"/>
</div>
<div id="main_content">
    <tiles:insertAttribute name="body"/>
</div>
<div id="footer">
    <tiles:insertAttribute name="footer"/>
</div>
</body>
</html>
