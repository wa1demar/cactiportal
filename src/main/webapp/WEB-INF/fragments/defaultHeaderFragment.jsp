<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  @author Vladimir Martynyuk
--%>

<div id="top_header">
    <div class="container">
        <img src="${pageContext.request.contextPath}/resources/images/logo.png"/>

        <div class="right_content">
            <div class="user_menu">
                <p><spring:message code="HEADER.user_menu.welcome"/>, <%= request.getUserPrincipal().getName() %>!</p>
                <ul class="sub_menu">
                    <li><a href="/secured/profile"><spring:message code="HEADER.user_menu.profile"/></a></li>
                    <li class="divider"></li>
                    <li><a href="/j_spring_security_logout"><spring:message code="HEADER.user_menu.logout"/></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="bottom_header">
    <div class="container">
        <div class="category_menu">
            <ul>
                <c:if test="${not empty userCategories}">
                    <c:forEach items="${userCategories}" var="c">
                        <li>
                            <a href="/secured/category/${c.id}"
                               <c:if test="${c.id == category.id}">class="current"</c:if>>
                                    ${c.name}
                            </a>
                        </li>
                    </c:forEach>
                </c:if>

                <li><a title="<spring:message code="HEADER.category_menu.add.hint" />" href="/secured/category/add"><i
                        class="glyphicon glyphicon-plus"></i></a>
                </li>

            </ul>
        </div>
    </div>
</div>
<div class="header_separator"></div>
<div class="flesh-messages">
    <c:if test="${ not empty success_flash_message }">
        <div class="alert alert-success">
            <div class="container">
                <span class="glyphicon glyphicon-ok-sign"></span>
                <c:out value="${success_flash_message}"/>
            </div>
        </div>
    </c:if>
    <c:if test="${ not empty error_flash_message }">
        <div class="alert alert-danger">
            <div class="container">
                <span class="glyphicon glyphicon-remove-sign"></span>
                <c:out value="${error_flash_message}"/>
            </div>
        </div>
    </c:if>
    <c:if test="${ not empty info_flash_message }">
        <div class="alert alert-info">
            <div class="container">
                <span class="glyphicon glyphicon-info-sign"></span>
                <c:out value="${info_flash_message}"/>
            </div>
        </div>
    </c:if>

</div>
