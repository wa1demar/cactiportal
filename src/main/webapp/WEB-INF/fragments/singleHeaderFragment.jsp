<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  @author Vladimir Martynyuk
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="top_header">
    <div class="container">
        <img src="${pageContext.request.contextPath}/resources/images/logo.png"/>

    </div>
</div>
<div id="bottom_header">
    <div class="container">
        <div class="category_menu">
            <ul>
                <li><a href="/login" class='<tiles:getAsString name="loginMenuClass" />'><spring:message code="LOGIN.menu.title.login" /></a></li>
                <li><a href="/register" class='<tiles:getAsString name="registerMenuClass" />'><spring:message code="LOGIN.menu.title.register" /></a></li>

            </ul>
        </div>
    </div>
</div>
<div class="header_separator"></div>
