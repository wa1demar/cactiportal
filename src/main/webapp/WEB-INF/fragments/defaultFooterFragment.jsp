<%--
  @author Vladimir Martynyuk
--%>

<div class="container">
    <p>2015 &copy; Ukrainian Cacti Portal</p>
    <p>Powered by <a target="_blank" href="http://spring.io" >Spring Framework</a></p>
</div>
