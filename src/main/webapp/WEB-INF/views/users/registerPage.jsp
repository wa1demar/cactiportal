<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  @author Vladimir Martynyuk
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<h1>${message}</h1>
<font color="red">
    ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
</font>

<div id="register_form">

    <f:form modelAttribute="user" method="post">
        <div class="form-group">
            <label class="required">Login</label>
            <f:input path="login" />
            <f:errors cssClass="label label-danger" path="login" />
        </div>
        <div class="form-group">
            <label class="required">Password</label>
            <f:input path="password" type="password" />
            <f:errors cssClass="label label-danger" path="password" />
        </div>
        <div class="form-group">
            <label class="required">Repeat Password</label>
            <f:input path="matchingPassword" type="password" />
            <f:errors cssClass="label label-danger" path="matchingPassword" />
            <f:errors />
        </div>

        <div class="form-group">
            <label class="required">Email</label>
            <f:input path="email" />
            <f:errors cssClass="label label-danger" path="email" />
        </div>

        <div class="form-group">
            <label>First Name</label>
            <f:input path="firstName" />
            <f:errors cssClass="label label-danger" path="firstName" />
        </div>

        <div class="form-group">
            <label>Last Name</label>
            <f:input path="lastName" />
            <f:errors cssClass="label label-danger" path="lastName" />
        </div>
        <input type="submit" class="right" value="Register"/>
    </f:form>

</div>
