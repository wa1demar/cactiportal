<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  @author Vladimir Martynyuk
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:if test="${ (not empty flash_message) }">
    <div class="alert alert-success" role="alert">
            ${flash_message}
    </div>
</c:if>

    <div class="error">
        Your login attempt was not successful, try again.<br />
        Reason: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
    </div>



<div id="login_form">
    <c:url value="/j_spring_security_check" var="loginUrl"/>

    <f:form action="${loginUrl}" method="post">
        <p>
            <label for="login"><spring:message code="LOGIN.label.login" /></label>
            <input type="text" id="login" name="login"/>
        </p>

        <p>
            <label for="password"><spring:message code="LOGIN.label.password" /></label>
            <input type="password" id="password" name="password"/>
        </p>

        <p>
            <input type="checkbox" name="_spring_security_remember_me" id="remember" value="true" class="css-checkbox"/>
            <label for="remember" class="css-label"><spring:message code="LOGIN.label.remember" /></label>
        </p>

        <input type="submit" class="right" value="<spring:message code="LOGIN.label.button.login" />"/>
    </f:form>

</div>
