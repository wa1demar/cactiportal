<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>

<%--
  @author Vladimir Martynyuk
--%>
<div class="container">
    <div id="content">
        <h1>Edit ${item.folder.name}</h1>
        <f:form commandName="item" modelAttribute="item" method="post"  action="/secured/item/${item.id}/edit" accept-charset="UTF-8">
            <f:hidden path="id"/>
            <div class="form-group">
                <f:label path="name" >Name</f:label>
                <f:errors path="name" cssClass="label label-danger" />
                <f:input path="name" />
            </div>
            <div class="form-group">
                <f:label path="itemKey" >Item Key:</f:label>
                <f:input path="itemKey" />
            </div>
            <div class="form-group">
                <f:label path="description" >Description</f:label>
                <f:textarea path="description" />
            </div>
            <div class="form-group">
                <a href="/secured/item/${currentItem.id}">Cancel</a>
                <input class="right" type="submit" value="Save" />
            </div>
        </f:form>
    </div>
</div>
<div class="separator" ></div>