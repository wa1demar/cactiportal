<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
  @author Vladimir Martynyuk
--%>

<div class="container">

    <div id="sidebar">
        <div id="category_tree">
            <ul>
                <li data-jstree='{"icon":"glyphicon glyphicon-folder-open", "opened":true}'>
                    <a href="/secured/category/<c:out value="${category.id}"/>"
                       title="<c:out value="${category.name}"/>">
                        <c:out value="${category.name}"/>
                    </a>
                    <ul>
                        <c:forEach items="${folders}" var="folder">
                            <li
                                    <c:choose>
                                    <c:when test="${folder.id == currentFolder.id}">data-jstree='{"icon":"glyphicon glyphicon-folder-open", "opened":true}'
                                    </c:when>
                                        <c:otherwise>data-jstree='{"icon":"glyphicon glyphicon-folder-close"}'</c:otherwise>
                            </c:choose>>
                                <a href="/secured/category/<c:out value="${category.id}"/>/<c:out value="${folder.id}"/>"
                                   title="<c:out value="${folder.name}"/>">
                                    <c:out value="${folder.name}"/>
                                </a>
                                <ul>
                                    <c:forEach items="${folder.items}" var="item">
                                        <li data-jstree='{"icon":"glyphicon glyphicon-leaf" <c:if test="${item.id == currentItem.id}" >, "selected":true</c:if>}'>
                                            <a href="/secured/item/<c:out value="${item.id}"/>"
                                               title="#<c:out value="${item.itemKey}"/> <c:out value="${item.name}"/>">
                                                <c:out value="${item.name}"/>
                                            </a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:forEach>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="content">
            <div class="toolbar" >
                <div class="tool-item" >
                    <h1>
                        <a  href="/secured/category/<c:out value="${currentItem.folder.category.id}" />">
                            <c:out value="${currentItem.folder.category.name}"/>
                        </a>
                    </h1>
                </div>
                <div class="tool-item" >
                    <h1>
                        <a  href="/secured/category/<c:out value="${currentItem.folder.category.id}" />/<c:out value="${currentItem.folder.category.id}" />">
                            <c:out value="${currentItem.folder.name}"/>
                        </a>
                    </h1>
                </div>
                <div class="tool-item" >
                    <h1><c:out value="${currentItem.name}"/></h1>
                </div>
                <div class="tool-item right" >
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="glyphicon glyphicon-cog"></i></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li><a href="/secured/item/${currentItem.id}/edit"><span class="glyphicon glyphicon-edit"></span><spring:message code="FOLDER.SHOW.menu.link.edit" /></a></li>
                        <li><a href="/secured/item/add"><span class="glyphicon glyphicon-plus"></span><spring:message code="FOLDER.SHOW.menu.link.add" /></a></li>
                        <li class="divider"></li>
                        <li><a href="/secured/item/${currentItem.id}/delete"><span class="glyphicon glyphicon-remove"></span><spring:message code="FOLDER.SHOW.menu.link.delete" /></a></li>
                    </ul>
                </div>
                <div class="tool-item right ${currentItem.isFavorite() ? 'current' : ''}">
                    <a href="#" ><i class="glyphicon glyphicon-star"></i></a>
                </div>
            </div>

        <c:if test="${currentItem.galleryItems.size() > 0}">
            <img class="logo"
                 src="${pageContext.request.contextPath}<c:out value="${currentItem.galleryItems[0].url}" />"/>
        </c:if>

        ${currentItem.description}

    </div>
</div>
<div class="separator" ></div>