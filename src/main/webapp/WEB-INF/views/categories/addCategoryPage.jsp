<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  @author Vladimir Martynyuk
--%>
<div class="container">
    <div id="content">
        <h1><spring:message code="CATEGORY.ADD.title" /></h1>
        <f:form commandName="category" modelAttribute="category" method="post"  action="/secured/category/add" accept-charset="UTF-8">
            <div class="form-group">
                <f:label path="name" ><spring:message code="CATEGORY.ADD.form.label.name" /></f:label>
                <f:errors path="name" cssClass="label label-danger" />
                <f:input path="name" />
            </div>
            <div class="form-group">
                <f:label path="description" ><spring:message code="CATEGORY.ADD.form.label.description" /></f:label>
                <f:textarea path="description" />
            </div>
            <div class="form-group">
                <input class="right" type="submit" value="<spring:message code="CATEGORY.ADD.form.label.save" />" />
            </div>
        </f:form>
    </div>
</div>
<div class="separator" ></div>

