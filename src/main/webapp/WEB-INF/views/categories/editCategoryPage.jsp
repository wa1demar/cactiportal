<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%--
  @author Vladimir Martynyuk
--%>
<div class="container">
    <div id="content">
        <h1>Edit Category</h1>
        <f:form commandName="category" modelAttribute="category" method="post"  action="/secured/category/${category.id}/edit" accept-charset="UTF-8">
            <f:hidden path="id"/>
            <div class="form-group">
                <f:label path="name" >Name</f:label>
                <f:input path="name" />
            </div>
            <div class="form-group">
                <f:label path="description" >Description</f:label>
                <f:textarea path="description" />
            </div>
            <div class="form-group">
                <input class="right" type="submit" value="Save" />
            </div>
        </f:form>
    </div>
</div>
<div class="separator" ></div>

