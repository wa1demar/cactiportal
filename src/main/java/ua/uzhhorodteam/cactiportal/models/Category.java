package ua.uzhhorodteam.cactiportal.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vladimir Martynyuk
 */

@Entity
@Table(name = "CATEGORY")
@NamedQueries(value = {
        @NamedQuery(name = "Category.findById", query = "FROM Category c WHERE c.id = :categoryId ORDER BY c.name ASC"),
        @NamedQuery(name = "Category.findByUserId", query = "FROM Category c WHERE c.user.id = :userId ORDER BY c.id ASC"),
        @NamedQuery(name = "Category.findByName", query = "FROM Category c WHERE c.name = :name ORDER BY c.id ASC"),
        @NamedQuery(name = "Category.findByNameAndUserName", query = "FROM Category c WHERE c.name = :name AND c.user.login = :userName ORDER BY c.id ASC"),
        @NamedQuery(name = "Category.findFirstUserCategory", query = "FROM Category c WHERE c.user.login = :userName ORDER BY c.id ASC")
})
public class Category implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "text")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    @OneToMany(mappedBy = "category")
    @OrderBy(value = "name")
    private Set<Folder> folders = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Folder> getFolders() {
        return folders;
    }

    public void setFolders(Set<Folder> folder) {
        this.folders = folder;
    }
}
