package ua.uzhhorodteam.cactiportal.models;

import ua.uzhhorodteam.cactiportal.enums.UserRole;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vladimir Martynyuk
 */

@Entity
@Table(name = "ROLE")
public class Role implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "TITLE")
    private UserRole roleName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getUserRole() {
        return roleName;
    }

    public void setUserRole(UserRole roleName) {
        this.roleName = roleName;
    }
}