package ua.uzhhorodteam.cactiportal.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Vladimir Martynyuk
 */

@Entity
@Table(name = "ITEM")
@NamedQueries(value = {
        @NamedQuery(name = "Item.findById", query = "from Item i where i.id = :id"),
        @NamedQuery(name = "Item.findMostViewedByCategory", query = "FROM Item i WHERE i.folder.category.id = :categoryId ORDER BY i.views DESC"),
        @NamedQuery(name = "Item.findMostViewedByFolder", query = "FROM Item i WHERE i.folder.id = :folderId ORDER BY i.views DESC"),
        @NamedQuery(name = "Item.findFavoritesByCategory", query = "FROM Item i WHERE i.folder.category.id = :categoryId AND i.isFavorite = true"),
        @NamedQuery(name = "Item.findFavoritesByFolder", query = "FROM Item i WHERE i.folder.id = :folderId AND i.isFavorite = true"),
        @NamedQuery(name = "Item.incrementViewsForItem", query = "UPDATE Item i SET i.views = i.views+1 WHERE i.id = :itemId")
})
public class Item implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "ITEM_KEY")
    private String itemKey;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION", columnDefinition = "text")
    private String description;

    @Column(name = "IS_FAVORITE", nullable = false, columnDefinition = "bit default false")
    private boolean isFavorite;

    @Column(name = "VIEWS")
    private Integer views;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FOLDER_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    private Folder folder;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", nullable = false)
    @Fetch(FetchMode.JOIN)
    private User user;

    @OneToMany(mappedBy = "item")
    @Fetch(FetchMode.JOIN)
    @OrderBy(value = "id")
    private List<GalleryItem> galleryItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemKey() {
        return itemKey;
    }

    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<GalleryItem> getGalleryItems() {
        return galleryItems;
    }

    public void setGalleryItems(List<GalleryItem> galleryItems) {
        this.galleryItems = galleryItems;
    }
}
