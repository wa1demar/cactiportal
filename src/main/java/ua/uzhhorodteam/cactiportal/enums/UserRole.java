package ua.uzhhorodteam.cactiportal.enums;

/**
 * @author Vladimir Martynyuk
 */
public enum  UserRole {
    ROLE_ADMIN,
    ROLE_USER
}
