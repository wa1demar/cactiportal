package ua.uzhhorodteam.cactiportal.enums;

/**
 * @author Vladimir Martynyuk
 */
public enum UserStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
