package ua.uzhhorodteam.cactiportal.handlers;

import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ua.uzhhorodteam.cactiportal.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Vladimir Martynyuk
 */

@Component
public class CategoryMenuViewPreparer implements ViewPreparer {

    @Override
    public void execute(Request request, AttributeContext attributeContext) {

        HttpServletRequest httpServletRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession session = httpServletRequest.getSession();

        User user = (User) session.getAttribute("user");

        request.getContext("request").put("userCategories", user.getCategories());

    }
}
