package ua.uzhhorodteam.cactiportal.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.uzhhorodteam.cactiportal.dao.CategoryDao;
import ua.uzhhorodteam.cactiportal.models.Category;

/**
 * @author Vladimir Martynyuk
 */
@Component
public class CategoryValidator implements Validator {

    @Autowired
    CategoryDao categoryDao;

    @Override
    public boolean supports(Class<?> clazz) {
        return Category.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
                "CATEGORY.ADD.validator.name.required", "Field name is required.");

        Category category = (Category)target;
        if (!category.getName().equals("") && !categoryNameIsUnique(category.getName())) errors.rejectValue("name",
                "CATEGORY.ADD.validator.name.unique", "Category Should Be Unique");
    }

    private boolean categoryNameIsUnique(String categoryName) {

        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        Category category = categoryDao.findByNameAndUserName(categoryName, userName);

        return category == null;
    }
}
