package ua.uzhhorodteam.cactiportal.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.uzhhorodteam.cactiportal.models.Item;

/**
 * @author Vladimir Martynyuk
 */

@Component
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Item.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
                "ITEM.ADD.validator.name.required", "Field name is required.");
    }
}
