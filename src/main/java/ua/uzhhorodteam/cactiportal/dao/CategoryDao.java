package ua.uzhhorodteam.cactiportal.dao;

import ua.uzhhorodteam.cactiportal.models.Category;

import java.util.List;

/**
 * @author Vladimir Martynyuk
 */
public interface CategoryDao {

    public List<Category> findAll();

    public Category findById(Long id);

    public Category findByName(String name);

    public Category findByNameAndUserName(String name, String userName);

    public List<Category> findByUserId(Long userId);

    public Category findFirstUserCategory(String userName);

    public Long saveOrUpdate(Category category);

    public void deleteCategory(Long id);

}
