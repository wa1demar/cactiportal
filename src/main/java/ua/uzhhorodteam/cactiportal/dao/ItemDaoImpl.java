package ua.uzhhorodteam.cactiportal.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.uzhhorodteam.cactiportal.models.Item;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Vladimir Martynyuk
 */

@Component
public class ItemDaoImpl implements ItemDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Item> findAll() {
        return null;
    }

    @Override
    @Transactional
    public List<Item> findMostViewedByCategory(Long categoryId) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.findMostViewedByCategory");
        query.setParameter("categoryId", categoryId);
        query.setMaxResults(10);

        @SuppressWarnings("unchecked")
        List<Item> itemList = (List<Item>) query.list();

        return itemList;
    }

    @Override
    @Transactional
    public List<Item> findMostViewedByFolder(Long folderId) {

        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.findMostViewedByFolder");
        query.setParameter("folderId", folderId);
        query.setMaxResults(10);

        @SuppressWarnings("unchecked")
        List<Item> itemList = (List<Item>) query.list();

        return itemList;
    }

    @Override
    @Transactional
    public List<Item> findFavoritesByFolder(Long folderId) {

        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.findFavoritesByFolder");
        query.setParameter("folderId", folderId);

        @SuppressWarnings("unchecked")
        List<Item> itemList = (List<Item>) query.list();

        return itemList;
    }

    @Override
    @Transactional
    public List<Item> findFavoritesByCategory(Long categoryId) {

        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.findFavoritesByCategory");
        query.setParameter("categoryId", categoryId);

        @SuppressWarnings("unchecked")
        List<Item> itemList = (List<Item>) query.list();

        return itemList;
    }

    @Override
    @Transactional
    public Item findById(Long id) {

        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.findById");
        query.setParameter("id", id);
        query.setMaxResults(1);

        @SuppressWarnings("unchecked")
        Item itemList = (Item) query.uniqueResult();

        return itemList;
    }

    @Override
    @Transactional
    public Item findByName(String login) {
        return null;
    }

    @Override
    @Transactional
    public void save(Item item) {

    }

    @Override
    @Transactional
    public Long update(Item item) {
        sessionFactory.getCurrentSession().update(item);

        return item.getId();
    }

    @Override
    @Transactional
    public void remove(Item item) {

    }

    @Override
    @Transactional
    public void remove(Long id) {

    }

    @Override
    @Transactional
    public void incrementViewsForItem(Long itemId) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Item.incrementViewsForItem");
        query.setParameter("itemId", itemId);

        query.executeUpdate();
    }
}
