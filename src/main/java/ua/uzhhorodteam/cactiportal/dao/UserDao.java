package ua.uzhhorodteam.cactiportal.dao;

import org.springframework.security.access.annotation.Secured;
import ua.uzhhorodteam.cactiportal.models.User;

import java.util.List;

/**
 * @author Vladimir Martynyuk
 */
public interface UserDao {

    public List<User> findAll();

    public User findById(Long id);

    public User findByName(String login);

    public void save(User user);

    public void update(User user);

    @Secured({"ROLE_ADMIN"})
    public void remove(User user);

    @Secured({"ROLE_ADMIN"})
    public void remove(Long id);
}