package ua.uzhhorodteam.cactiportal.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.uzhhorodteam.cactiportal.models.Category;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladimir Martynyuk
 */

@Component
public class CategoryDaoImpl implements CategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Category> findAll() {
        List<Category> categoryList = new ArrayList<>();

            categoryList = sessionFactory.getCurrentSession().createCriteria(Category.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        return categoryList;
    }

    @Override
    @Transactional
    public Category findById(Long categoryId) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Category.findById");
        query.setParameter("categoryId", categoryId);

        @SuppressWarnings("unchecked")
        Category category = (Category) query.uniqueResult();

        return category;
    }

    @Override
    @Transactional
    public Category findByName(String name) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Category.findByName");
        query.setParameter("name", name);

        @SuppressWarnings("unchecked")
        Category category = (Category) query.uniqueResult();

        return category;
    }

    @Override
    @Transactional
    public Category findByNameAndUserName(String name, String userName) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Category.findByNameAndUserName");
        query.setParameter("name", name);
        query.setParameter("userName", userName);

        @SuppressWarnings("unchecked")
        Category category = (Category) query.uniqueResult();

        return category;
    }

    @Override
    @Transactional
    public List<Category> findByUserId(Long userId) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Category.findByUserId");
        query.setParameter("userId", userId);

        @SuppressWarnings("unchecked")
        List<Category> categoryList = (List<Category>) query.list();

        return categoryList;
    }

    @Override
    @Transactional
    public Category findFirstUserCategory(String userName) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("Category.findFirstUserCategory");
        query.setParameter("userName", userName);
        query.setMaxResults(1);

        @SuppressWarnings("unchecked")
        Category category = (Category) query.uniqueResult();

        return category;
    }

    @Override
    @Transactional
    public Long saveOrUpdate(Category category) {
        sessionFactory.getCurrentSession().saveOrUpdate(category);

        return category.getId();
    }

    @Override
    @Transactional
    public void deleteCategory(Long id) {
        Category category = findById(id);
        sessionFactory.getCurrentSession().delete(category);
    }
}
