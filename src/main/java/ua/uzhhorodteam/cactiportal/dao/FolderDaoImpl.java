package ua.uzhhorodteam.cactiportal.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.uzhhorodteam.cactiportal.models.Category;
import ua.uzhhorodteam.cactiportal.models.Folder;
import ua.uzhhorodteam.cactiportal.models.User;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Vladimir Martynyuk
 */
@Component
public class FolderDaoImpl implements FolderDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<Folder> findAll() {
        return null;
    }

    @Override
    @Transactional
    public Folder findById(Long folderId) {
        String hql = "from Folder f where f.id = :folderId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("folderId", folderId);
        query.setMaxResults(1);

        @SuppressWarnings("unchecked")
        List<Folder> folderList = (List<Folder>) query.list();

        return folderList.get(0);
    }

    @Override
    @Transactional
    public List<Folder> findByCategoryId(Long categoryId) {

        String hql = "from Folder f where f.category.id = :categoryId";
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameter("categoryId", categoryId);

        @SuppressWarnings("unchecked")
        List<Folder> folderList = (List<Folder>) query.list();

        return folderList;
    }

    @Override
    @Transactional
    public Folder findByName(String name) {
        return null;
    }

    @Override
    @Transactional
    public List<Folder> findByUserId(Long userId) {
        return null;
    }
}
