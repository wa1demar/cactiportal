package ua.uzhhorodteam.cactiportal.dao;




import ua.uzhhorodteam.cactiportal.models.Folder;

import java.util.List;

/**
 * @author Vladimir Martynyuk
 */
public interface FolderDao {

    public List<Folder> findAll();

    public Folder findById(Long id);

    public List<Folder> findByCategoryId(Long categoryId);

    public Folder findByName(String name);

    public List<Folder> findByUserId(Long userId);
}
