package ua.uzhhorodteam.cactiportal.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.uzhhorodteam.cactiportal.exceptions.DaoSystemException;
import ua.uzhhorodteam.cactiportal.exceptions.NoSuchEntityException;
import ua.uzhhorodteam.cactiportal.models.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladimir Martynyuk
 */

@Component
public class RoleDaoImpl implements RoleDao {

    @Autowired
    private SessionFactory sessionFactory;

    public RoleDaoImpl() {
    }

    public RoleDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> findAll() throws DaoSystemException  {
        List<Role> categoryList = new ArrayList<>();

        try {
            categoryList = sessionFactory.getCurrentSession().createCriteria(Role.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return categoryList;
    }

    @Override
    public Role findById(Long id) throws DaoSystemException, NoSuchEntityException {
        return null;
    }

    @Override
    public void saveOrUpdate(Role role) {

    }

    @Override
    public void remove(Role role) {

    }

    @Override
    public void remove(Long id) throws DaoSystemException, NoSuchEntityException {

    }
}
