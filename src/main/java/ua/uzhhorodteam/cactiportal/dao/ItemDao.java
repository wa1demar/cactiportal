package ua.uzhhorodteam.cactiportal.dao;

import ua.uzhhorodteam.cactiportal.models.Item;

import java.util.List;

/**
 * @author Vladimir Martynyuk
 */
public interface ItemDao {

    public List<Item> findAll();

    public List<Item> findMostViewedByCategory(Long categoryId);

    public List<Item> findMostViewedByFolder(Long folderId);

    public List<Item> findFavoritesByFolder(Long folderId);

    public List<Item> findFavoritesByCategory(Long categoryId);

    public Item findById(Long id);

    public Item findByName(String login);

    public void save(Item item);

    public Long update(Item item);

    public void remove(Item item);

    public void remove(Long id);

    public void incrementViewsForItem(Long itemId);
}
