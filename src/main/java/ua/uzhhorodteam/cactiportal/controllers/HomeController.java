package ua.uzhhorodteam.cactiportal.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Vladimir Martynyuk
 */

@Controller
@RequestMapping("/")
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRedirectToSecuredPage() {
        return "redirect:/secured/";
    }


}
