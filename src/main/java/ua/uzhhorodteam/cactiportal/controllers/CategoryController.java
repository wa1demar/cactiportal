package ua.uzhhorodteam.cactiportal.controllers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.uzhhorodteam.cactiportal.dao.CategoryDao;
import ua.uzhhorodteam.cactiportal.dao.FolderDao;
import ua.uzhhorodteam.cactiportal.dao.ItemDao;
import ua.uzhhorodteam.cactiportal.dao.UserDao;
import ua.uzhhorodteam.cactiportal.handlers.CategoryMenuViewPreparer;
import ua.uzhhorodteam.cactiportal.models.Category;
import ua.uzhhorodteam.cactiportal.models.Folder;
import ua.uzhhorodteam.cactiportal.models.Item;
import ua.uzhhorodteam.cactiportal.models.User;
import ua.uzhhorodteam.cactiportal.validators.CategoryValidator;

import java.util.List;
import java.util.Locale;


/**
 * @author Vladimir Martynyuk
 */

@Controller
public class CategoryController {

    Logger logger = LogManager.getLogger(CategoryController.class);

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    FolderDao folderDao;

    @Autowired
    ItemDao itemDao;

    @Autowired
    UserDao userDao;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    @Qualifier("categoryValidator")
    private CategoryValidator validator;

    @RequestMapping(value = "/secured/", method = RequestMethod.GET)
    public String getRedirectToCategoryPage(final RedirectAttributes redirectAttributes) {
        logger.info("GET: /secured/");

        return redirectToUserCategory(redirectAttributes);

    }

    @RequestMapping(value = "/secured/category/{categoryId}", method = RequestMethod.GET)
    public String getCategoryPage(@PathVariable("categoryId") Integer categoryId, ModelMap model) {
        logger.info("GET: /secured/category/" + categoryId);

        Category category = categoryDao.findById(Long.valueOf(categoryId));

        List<Folder> folders = folderDao.findByCategoryId(Long.valueOf(categoryId));

        List<Item> favorites = itemDao.findFavoritesByCategory(Long.valueOf(categoryId));
        List<Item> popularItems = itemDao.findMostViewedByCategory(Long.valueOf(categoryId));

        model.addAttribute("category", category);
        model.addAttribute("folders", folders);
        model.addAttribute("favorites", favorites);
        model.addAttribute("popularItems", popularItems);

        return "CategoryPage";
    }

    @RequestMapping(value = "/secured/category/{categoryId}/{folderId}", method = RequestMethod.GET)
    public String getFolderPage(@PathVariable("categoryId") Integer categoryId, @PathVariable("folderId") Integer folderId, ModelMap model) {
        logger.info("GET: /secured/category/" + categoryId + "/" + folderId);

        Category category = categoryDao.findById(Long.valueOf(categoryId));

        Folder currentFolder = folderDao.findById(Long.valueOf(folderId));

        List<Folder> folders = folderDao.findByCategoryId(Long.valueOf(categoryId));

        List<Item> favorites = itemDao.findFavoritesByFolder(Long.valueOf(folderId));
        List<Item> popularItems = itemDao.findMostViewedByFolder(Long.valueOf(folderId));

        model.addAttribute("category", category);
        model.addAttribute("currentFolder", currentFolder);
        model.addAttribute("folders", folders);
        model.addAttribute("favorites", favorites);
        model.addAttribute("popularItems", popularItems);

        return "FolderPage";

    }

    @RequestMapping(value = "/secured/category/{categoryId}/edit", method = RequestMethod.GET)
    public String getEditCategoryPage(@PathVariable("categoryId") Integer categoryId, ModelMap model) {
        logger.info("GET: /secured/category/" + categoryId + "/edit");

        Category category = categoryDao.findById(Long.valueOf(categoryId));

        List<Folder> folders = folderDao.findByCategoryId(Long.valueOf(categoryId));

        List<Item> favorites = itemDao.findFavoritesByCategory(Long.valueOf(categoryId));
        List<Item> popularItems = itemDao.findMostViewedByCategory(Long.valueOf(categoryId));

        model.addAttribute("category", category);
        model.addAttribute("folders", folders);
        model.addAttribute("favorites", favorites);
        model.addAttribute("popularItems", popularItems);

        return "EditCategoryPage";
    }

    @RequestMapping(value = "/secured/category/{categoryId}/edit", method = RequestMethod.POST)
    public String postEditCategoryPage(@PathVariable("categoryId") Integer categoryId, @ModelAttribute("category") Category category,
                                       final RedirectAttributes redirectAttributes) {
        logger.info("POST: /secured/category/" + categoryId + "/edit");

        User currentUser = getCurrentUser();

        category.setUser(currentUser);
        Long newCategoryId = categoryDao.saveOrUpdate(category);

        redirectAttributes.addFlashAttribute("success_flash_message", "Category \"" + category.getName() + "\" has been saved");

        return "redirect:/secured/category/" + newCategoryId;
    }

    @RequestMapping(value = "/secured/category/add", method = RequestMethod.GET)
    public String getAddCategoryPage(ModelMap model) {
        logger.info("GET: /secured/category/add");

        Category category = new Category();

        model.addAttribute("category", category);
        return "AddCategoryPage";
    }

    @RequestMapping(value = "/secured/category/add", method = RequestMethod.POST)
    public String postAddCategoryPage(@ModelAttribute("category") Category category, final RedirectAttributes redirectAttributes, BindingResult result) {
        logger.info("POST: /secured/category/add");

        validator.validate(category, result);

        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            StringBuilder errorsString = new StringBuilder();
            for (ObjectError error : errors) {
                errorsString.append(error.getDefaultMessage()).append("<br />");
            }
            redirectAttributes.addFlashAttribute("error_flash_message", errorsString.toString());

            return "AddCategoryPage";

        } else {

            User currentUser = getCurrentUser();

            category.setUser(currentUser);
            Long newCategoryId = categoryDao.saveOrUpdate(category);

            redirectAttributes.addFlashAttribute("success_flash_message", "Category \"" + category.getName() + "\" has been added");

            return "redirect:/secured/category/" + newCategoryId;
        }
    }

    @RequestMapping(value = "/secured/category/{categoryId}/delete", method = RequestMethod.GET)
    public String getDeleteCurrentCategory(@PathVariable("categoryId") Integer categoryId, final RedirectAttributes redirectAttributes, Locale loc) {
        categoryDao.deleteCategory(Long.valueOf(categoryId));

        redirectAttributes.addFlashAttribute("success_flash_message", messageSource.getMessage("CATEGORY.REMOVE.success.message", null, loc));

        return redirectToUserCategory(redirectAttributes);
    }

    private User getCurrentUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        return userDao.findByName(username);
    }

    private String redirectToUserCategory(RedirectAttributes redirectAttributes) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        Category category = categoryDao.findFirstUserCategory(userName);

        if (category != null) {
            return "redirect:/secured/category/" + category.getId();
        } else {
            redirectAttributes.addFlashAttribute("info_flash_message", "Add your first category");
            return "redirect:/secured/category/add";
        }
    }
}
