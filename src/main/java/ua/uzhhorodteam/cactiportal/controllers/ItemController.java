package ua.uzhhorodteam.cactiportal.controllers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.uzhhorodteam.cactiportal.dao.CategoryDao;
import ua.uzhhorodteam.cactiportal.dao.FolderDao;
import ua.uzhhorodteam.cactiportal.dao.ItemDao;
import ua.uzhhorodteam.cactiportal.models.Category;
import ua.uzhhorodteam.cactiportal.models.Folder;
import ua.uzhhorodteam.cactiportal.models.Item;
import ua.uzhhorodteam.cactiportal.validators.ItemValidator;

import java.util.List;

/**
 * @author Vladimir Martynyuk
 */

@Controller
@RequestMapping(value = "/secured/item/")
public class ItemController {

    Logger logger = LogManager.getLogger(ItemController.class);

    @Autowired
    CategoryDao categoryDao;

    @Autowired
    FolderDao folderDao;

    @Autowired
    ItemDao itemDao;

    @Autowired
    @Qualifier("itemValidator")
    private ItemValidator validator;

    @RequestMapping(value = "{itemId}", method = RequestMethod.GET)
    public String getItemPage(@PathVariable("itemId") Integer itemId, ModelMap model, RedirectAttributes redirectAttributes) {
        logger.debug("/secured/item" + itemId);

        Item currentItem = itemDao.findById(Long.valueOf(itemId));

        if (currentItem == null) {
            redirectAttributes.addFlashAttribute("error_flash_message", "Item with ID " + itemId + " not found!");

            return "redirect:/secured/category";
        }

        itemDao.incrementViewsForItem(currentItem.getId());

        Category category = categoryDao.findById(currentItem.getFolder().getCategory().getId());

        Folder currentFolder = currentItem.getFolder();

        List<Folder> folders = folderDao.findByCategoryId(category.getId());

        model.addAttribute("currentItem", currentItem);
        model.addAttribute("category", category);
        model.addAttribute("currentFolder", currentFolder);
        model.addAttribute("folders", folders);
//        model.addAttribute("galleryItem", currentItem.getGalleryItems().get(0));

        return "ItemPage";
    }

    @RequestMapping(value = "{itemId}/edit", method = RequestMethod.GET)
    public String getEditItemPage(@PathVariable("itemId") Integer itemId, ModelMap model,
                                  RedirectAttributes redirectAttributes) {

        logger.info("GET: /secured/item/" + itemId + "/edit");

        Item item = itemDao.findById(Long.valueOf(itemId));

        if (item == null) {
            redirectAttributes.addFlashAttribute("error_flash_message", "Item with ID " + itemId + " not found!");
            return "redirect:/secured/category";
        } else {
            model.addAttribute("item", item);
        }

        return "EditItemPage";
    }

    @RequestMapping(value = "{itemId}/edit", method = RequestMethod.POST)
    public String postEditItemPage(@PathVariable("itemId") Integer itemId, @ModelAttribute("item") Item item,
                                  RedirectAttributes redirectAttributes, BindingResult result) {
        logger.info("POST: /secured/item/" + itemId + "/edit");

        validator.validate(item, result);

        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            StringBuilder errorsString = new StringBuilder();
            for (ObjectError error : errors) {
                errorsString.append(error.getDefaultMessage()).append("<br />");
            }
            redirectAttributes.addFlashAttribute("error_flash_message", errorsString.toString());

            return "EditItemPage";
        } else {
            Item currentItem = itemDao.findById(Long.valueOf(itemId));
            currentItem.setName(item.getName());
            currentItem.setItemKey(item.getItemKey());
            currentItem.setDescription(item.getDescription());
            Long newItemId = itemDao.update(currentItem);

            redirectAttributes.addFlashAttribute("success_flash_message", "\"" + currentItem.getFolder().getName() + " " + currentItem.getName() + "\" has been saved");

            return "redirect:/secured/item/" + newItemId;
        }

    }


}
