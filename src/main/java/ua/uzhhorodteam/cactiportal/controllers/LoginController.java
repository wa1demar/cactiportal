package ua.uzhhorodteam.cactiportal.controllers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.uzhhorodteam.cactiportal.dao.UserDao;
import ua.uzhhorodteam.cactiportal.models.User;

/**
 * @author Vladimir Martynyuk
 */

@Controller
public class LoginController {

    Logger logger = LogManager.getLogger(LoginController.class);

    @Autowired
    UserDao userDao;

    @Autowired
    @Qualifier("registrationFormValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(ModelMap model) {
        logger.info("GET: /login");

        return "LoginPage";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterPage(ModelMap model) {
        logger.info("GET: /register");

        User user = new User();

        model.addAttribute("user", user);

        return "RegisterPage";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String postRegisterPage(@Validated @ModelAttribute("user") User user, BindingResult result,
                                   final RedirectAttributes redirectAttributes, ModelMap model) {
        logger.info("POST: /register");

        if (result.hasErrors()) {
            return "RegisterPage";
        } else {

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);
            User newUser = user;
            newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userDao.save(newUser);
            redirectAttributes.addFlashAttribute("flash_message", "user added");
            return "redirect:/login";
        }

    }
}
