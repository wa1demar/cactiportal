package ua.uzhhorodteam;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Vladimir Martynyuk
 */
public class PasswordEncoder {

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
        System.out.println("admin: " + encoder.encode("admin"));
        System.out.println("moder: " + encoder.encode("moder"));
        System.out.println("viewer: " + encoder.encode("viewer"));
        System.out.println("qwerty11: " + encoder.encode("qwerty11"));

    }
}
